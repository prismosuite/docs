<!-- MarkdownTOC -->

- [What is Prismo?](#what-is-prismo)
- [What is federation in the context of Prismo?](#what-is-federation-in-the-context-of-prismo)
- [What are the differences between Prismo and other link-aggregation sites, such as reddit?](#what-are-the-differences-between-prismo-and-other-link-aggregation-sites-such-as-reddit)
- [Is Prismo FLOSS? What is the license?](#is-prismo-floss-what-is-the-license)
- [Can anyone create a new Prismo instance?](#can-anyone-create-a-new-prismo-instance)
- [What are the rules for each Prismo instance?](#what-are-the-rules-for-each-prismo-instance)
- [How can I help making the internet ~~great~~ free again?](#how-can-i-help-making-the-internet-%7E%7Egreat%7E%7E-free-again)
- [Is there a directory of all the Prismo instances?](#is-there-a-directory-of-all-the-prismo-instances)
- [What is Prismo's roadmap?](#what-is-prismos-roadmap)
- [Is there a main instance?](#is-there-a-main-instance)
- [Is there a forum and/or an IRC channel?](#is-there-a-forum-andor-an-irc-channel)
- [Why are there no downvotes in Prismo?](#why-are-there-no-downvotes-in-prismo)

<!-- /MarkdownTOC -->


## What is Prismo?

Prismo is a federated link aggregation service powered by [ActivityPub](https://en.wikipedia.org/wiki/ActivityPub).


## What is federation in the context of Prismo?

In this context, federation means that different Prismo instances/services can operate collectively, that is, can communicate between themselves. They can also operate with other Activity-Pub services, such as mastodon or friendica. An example of a federated system is the e-mail protocol. You can have an e-mail account in a different network than your friend and still communicate with him. For example, you use gmail.com and your friend uses hotmail. Another example of that is [mastodon](https://joinmastodon.org/).


## What are the differences between Prismo and other link-aggregation sites, such as reddit?

The main difference is that Prismo is federated/decentralized. So, different people can run different instances and communicate between themselves! Since each instance only hosts their own-users and the content to which they are subscribed, it is very scalable and efficient. On top of that, If you are not happy with the moderation of a given instance, you can create your instance with your own ~~blackjack and hookers~~ rules.


## Is Prismo FLOSS? What is the license?

Yes, Prismo is **F**ree **L**ibre **O**pen-**S**ource **S**oftware! The license is the *GNU Affero General Public License*. The source-code for prismo is available [here](https://gitlab.com/mbajur/prismo/).


## Can anyone create a new Prismo instance?

Yes, that is the whole point! As long as you have the minimal technical expertise, you should be able to create a new Prismo instance. However, please keep in mind that if your instance behaves badly it may be be blocked from other Prismo instances. We suggest opening instances for topics that fascinate you. For example, if you're into gardening, why not open a "prismo-gardening" instance?

You can read on how to setup a new instance [here](https://gitlab.com/mbajur/prismo#deployment).


## What are the rules for each Prismo instance?

That is decided by the admin of each Prismo instance.


## How can I help making the internet ~~great~~ free again?

If you can code, you can start by solving some [issues](https://gitlab.com/mbajur/prismo/issues). You can help translate Prismo to different languages [here](Where?). You can also just participate in discussions, send some interesting links and tell your friends about us!

If you want to help financially, you can donate in one of the following platforms:

- [Patreon](https://www.patreon.com/mxbx)
- [Liberapay](https://liberapay.com/mxbx/)

There is more info regarding contributions [here](https://gitlab.com/mbajur/prismo/blob/master/CONTRIBUTING.md).

## Is there a directory of all the Prismo instances?

There is some information [here](https://the-federation.info/prismo). In the future, this information will be in a prettier site.

## What is Prismo's roadmap?

You can see Prismo's roadmap [here](link_to_roadmap_in_README).

## Is there a main instance?

Yes, the flagship instance is https://prismo.news/.

## Is there a forum and/or an IRC channel?

There is a forum [here](https://socialhub.network/c/prismo) and there is a [Matrix](https://www.matrix.org) channel: #prismo:matrix.org.


## Why are there no downvotes in Prismo?

There are some communities that have downvotes and there are others that do not. An example of a community that has downvotes is [reddit](https://www.reddit.com/). An example of one that does not have downvotes (at least not globally) is [HN](https://news.ycombinator.com/). So, not having downvotes is not ground-breaking, it has been done before and it does not break a community.

The main idea for not having downvotes is that downvotes serve as a deterrent to discussions. If one doesn't like a comment, one should be able to freely and eloquently explain their reasoning without resorting to downvoting it. There is no need to downvote as that is not "discussion friendly". If one likes a comment, then one should upvote it. 

There is also the issue with downvote bridages. Many communities have solved that by hiding the score. Not having downvotes also solves this problem.

If, however, a comment is so inhospitable that it goes against the rules, then there is a "flag" feature, which should be used. An example in which this feature should be used is for pure spam, either in comments or posts.

Summing up:

- If a comment has created a discussion around it, then it is a comment that does not deserve to be downvoted;
- If a comment has not helped the discussion but there is really nothing wrong with it, then just leave it be without upvoting it;
- If a comment is against the rules, then flag it;
